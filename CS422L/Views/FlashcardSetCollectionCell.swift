//
//  FlashCardSetCollectionCell.swift
//  CS422L
//
//  Created by Jonathan Sligh on 2/3/21.
//

import Foundation
import UIKit

class FlashcardSetCollectionCell: UICollectionViewCell
{
    @IBOutlet weak var setLabel: UILabel!
    
    func configure(with setText: String) {
        setLabel.text = setText
    }
    
    
}
