//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {

    var sets: [FlashcardSet] = FlashcardSet.getHardCodedCollection()
    
    @IBOutlet weak var setCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //connect hard coded collection to sets
        setCollectionView.delegate = self
        setCollectionView.dataSource = self
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //return number of items
        return sets.count
    }
    
    //configures the cell and returns it to the view
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup cell display here
        cell.layer.cornerRadius = 8
        cell.configure(with: sets[indexPath.row].title)
        return cell
    }
    
    //segue to FlashcardSetDetail Activity
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "toSetDetailSegue", sender: self)
        
    }
    //adds a new set to main screen
    @IBAction func addButton(_ sender: Any) {
        let newset = FlashcardSet()
        newset.title = "Set " + String(sets.count + 1)
        sets.append(newset)
        self.setCollectionView.reloadData()
        print("Added")
    }
}

