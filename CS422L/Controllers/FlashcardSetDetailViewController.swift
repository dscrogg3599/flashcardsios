//
//  FlashcardSetDetailViewController.swift
//  CS422L
//
//  Created by Daniel Scroggins on 1/31/22.
//

import UIKit

class FlashcardSetDetailViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var topStackView: UIStackView!
    @IBOutlet var table: UITableView!
    var cards: [Flashcard] = Flashcard.getHardCodedFlashcards()

    override func viewDidLoad() {
        super.viewDidLoad()
        table.delegate = self
        table.dataSource = self
        topStackView.layer.cornerRadius = 8

        // Do any additional setup after loading the view.
    }
    
    //returns count of cards
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count
    }
    
    //configures cell and returns it to view
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Card Cell", for: indexPath)
        cell.textLabel?.text = cards[indexPath.row].term
        return cell
    }
    
    //adds a new flashcard to the list of flashcards
    @IBAction func addButton(_ sender: Any) {
        let newcard = Flashcard()
        newcard.term = "Term " + String(cards.count + 1)
        newcard.definition = "Testing"
        cards.append(newcard)
        self.table.reloadData()
        
        print("Added")
    }
    
    //allows rows to be editable
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //slide to delete function
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "Delete") {action, indexPath in
            self.cards.remove(at: indexPath.row)
            self.table.deleteRows(at: [indexPath], with: .automatic)
        }
        
        return [deleteAction]
    }
    
}
